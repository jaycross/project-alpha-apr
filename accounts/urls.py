from django.urls import path
from accounts.views import login_form, user_logout, signupform

urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signupform, name="signup"),
]
