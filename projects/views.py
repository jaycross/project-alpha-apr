from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_all(request: HttpRequest) -> HttpResponse:
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/my_projects.html", context)


@login_required
def show_project(request, id: int):
    tasks = Task.objects.filter(project=id)
    projects = Project.objects.get(id=id)
    context = {"tasks": tasks, "projects": projects}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            proj_form = form.save(False)
            proj_form.owner = request.user
            proj_form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
