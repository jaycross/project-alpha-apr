from django.urls import path
from .views import project_all, show_project, create_project

urlpatterns = [
    path("", project_all, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
