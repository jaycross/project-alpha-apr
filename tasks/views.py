from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from .models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        taskform = TaskForm(request.POST)
        if taskform.is_valid():
            taskform.save()
            return redirect("list_projects")
    else:
        taskform = TaskForm()
    return render(request, "create.html", {"taskform": taskform})


@login_required
def assignee_view(request):
    assigns = Task.objects.filter(assignee=request.user)
    context = {"assigns": assigns}
    return render(request, "list.html", context)
