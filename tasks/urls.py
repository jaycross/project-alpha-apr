from django.urls import path
from .views import create_task, assignee_view

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", assignee_view, name="show_my_tasks"),
]
